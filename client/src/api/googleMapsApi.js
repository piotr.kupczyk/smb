import { Loader } from 'google-maps'

const GOOGLE_CONSOLE_KEY = process.env.GOOGLE_CLOUD_API_KEY

const loader = new Loader(GOOGLE_CONSOLE_KEY).load()

export {
  loader
}
