import firebase from 'firebase'

// firebase init - add your own config here
const firebaseConfig = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.FIREBASE_APP_ID,
  measurementId: process.env.FIREBASE_MEASUREMENT_ID
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig)
firebase.analytics()

// utils
const db = firebase.firestore()
const auth = firebase.auth

// collection references
// const productsCollection = db.collection('users')
const productsCollection = db.collection('products')
const favouritesShopsCollection = db.collection('favouritesShops')

// export utils/refs
export {
  db,
  auth,
  productsCollection,
  favouritesShopsCollection
}
