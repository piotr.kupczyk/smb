import { favouritesShopsCollection } from 'src/api/firebase'

const favouritesShopsRepository = {
  async getAll () {
    return (await favouritesShopsCollection.get()).docs.map(p => {
      return {
        id: p.id,
        ...p.data()
      }
    })
  },
  async getById (id) {
    const shop = await favouritesShopsCollection.doc(id).get()
    return {
      id: id,
      ...shop.data()
    }
  },
  async save (product, id) {
    let createdShopRef
    if (id === null) {
      createdShopRef = await favouritesShopsCollection.add(product)
      await favouritesShopsCollection.doc(createdShopRef).set({
        id: createdShopRef,
        ...product
      })
    } else {
      await favouritesShopsCollection.doc(id).set(product)
      createdShopRef = id
    }
    return this.getById(createdShopRef.id)
  },
  async removeById (productId) {
    await favouritesShopsCollection.doc(productId).delete()
  }
}

export { favouritesShopsRepository }
