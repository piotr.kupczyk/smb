import { productsCollection } from 'src/api/firebase'

const productRepository = {
  async getAll () {
    return (await productsCollection.get()).docs.map(p => {
      return {
        id: p.id,
        ...p.data()
      }
    })
  },
  async getById (id) {
    const product = await productsCollection.doc(id).get()
    return {
      id: id,
      ...product.data()
    }
  },
  async save (product, id) {
    let createdProductRef
    if (id === null) {
      createdProductRef = await productsCollection.add(product)
    } else {
      await productsCollection.doc(id).set(product)
      createdProductRef = id
    }
    return this.getById(createdProductRef.id)
  },
  async removeById (productId) {
    await productsCollection.doc(productId).delete()
  }
}

export { productRepository }
