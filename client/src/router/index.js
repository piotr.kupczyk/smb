import Vue from 'vue'
import VueRouter from 'vue-router'
import VueLocalStorage from 'vue-localstorage'
import VueGeolocation from 'vue-browser-geolocation/src'

import routes from './routes'
import { auth } from 'src/api/firebase'

Vue.use(VueRouter)
Vue.use(VueLocalStorage)
Vue.use(VueGeolocation)
// Vue.use(VueGoogleMaps, {
//   load: {
//     key: process.env.GOOGLE_CLOUD_API_KEY,
//     libraries: 'places,drawing,visualization',
//     v: '3.26'
//   },
//   installComponents: true
// })
/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(x => x.meta.requiresAuth)
    const currentUser = auth().currentUser
    if (requiresAuth && !currentUser) {
      next('/login')
    } else {
      next()
    }
  })

  return Router
}
