const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/ProductListPage.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'add',
        name: 'add',
        component: () => import('pages/AddEditProductPage.vue'),
        props: route => ({ id: route.query.id }),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'settings',
        component: () => import('pages/SettingsPage.vue')
      },
      {
        path: '/login',
        name: 'Login',
        component: () => import('pages/LoginPage.vue')
      },
      {
        path: '/shop',
        name: 'Shops',
        component: () => import('pages/ShopListPage')
        // meta: {
        //   requiresAuth: true
        // },
      },
      {
        path: '/shop/find',
        name: 'ShopsMap',
        component: () => import('pages/FavouritesShopsPage.vue')
        // meta: {
        //   requiresAuth: true
        // }
      }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/static/Error404.vue')
  }
]

export default routes
