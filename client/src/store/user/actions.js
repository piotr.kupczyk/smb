const setUserProfile = ({ commit }, userProfile) => {
  commit('SET_USER_PROFILE', userProfile)
}

export {
  setUserProfile
}
