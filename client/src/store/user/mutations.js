const SET_USER_PROFILE = (state, profile) => {
  state.userProfile = profile
}

export {
  SET_USER_PROFILE
}
