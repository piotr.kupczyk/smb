/*
 * This file (which will be your service worker)
 * is picked up by the build system ONLY if
 * quasar.conf > pwa > workboxPluginMode is set to "InjectManifest"
 */
// import { register } from 'register-service-worker'
import { precacheAndRoute } from 'workbox-precaching'
// import { convertedKey } from 'app/src-pwa/push/PushService'
// // The ready(), registered(), cached(), updatefound() and updated()
// // events passes a ServiceWorkerRegistration instance in their arguments.
// // ServiceWorkerRegistration: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration
precacheAndRoute(self.__WB_MANIFEST)

self.addEventListener('beforeinstallprompt', function (e) {
  e.preventDefault()
  console.log(e)
  e.prompt()
})

self.addEventListener('push', function (event) {
  console.log(`Received event: ${JSON.stringify(event.data.json())}`)
  const data = event.data.json()
  const notificationPromise = self.registration.showNotification(data.title, {
    body: 'Tap to see more details...',
    icon: 'https://www.flaticon.com/svg/static/icons/svg/2625/2625106.svg',
    data: event.data.json().body,
    click_action: `http://192.168.0.130:8080/add?id=${event.data.json().body.id}`
  })
  event.waitUntil(notificationPromise)
})

self.addEventListener('notificationclick', function (event) {
  const newProduct = event.notification.data
  console.log(newProduct)
  event.notification.close()
  event.waitUntil(
    self.clients.openWindow(`http://192.168.0.130:8080/#/add?id=${newProduct.id}`)
  )
})
