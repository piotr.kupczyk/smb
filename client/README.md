# SMB

### Mini-projekt 1

MainLayout -> MainActivity

ProductListPage -> ProductListActivity

SettingsPage -> OptionsActivity

Dane produktow oraz opcji przechowywane są w localStorage, co zapewnia persystencję danych pomiędzy uruchomieniami.

### How to run locally? REQUIRES quasar-cli installed (docker configuration in progress)
```quasar dev -m pwa```

