#!/bin/bash

quasar build -m pwa || exit 1

docker build -t piotrkupczyk/smb/client . || exit 1

exit 0;
