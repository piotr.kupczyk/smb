#!/bin/bash

# Build client docker image

#cd client
#
#quasar build -m pwa || exit 1
#
#docker build -t piotrkupczyk/smb/client . || exit 1
#
## Build server docker image
#
#cd ..

docker build -t piotrkupczyk/smb/server ./server || exit 1

exit 0;