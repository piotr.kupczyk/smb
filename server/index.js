
const express = require("express");
const webpush = require("web-push");
const bodyParser = require("body-parser");
const path = require("path");
const cors = require('cors');

const app = express();


// Set static path
app.use(express.static(path.join(__dirname, "client")));
app.use(cors({
    origin: '*'
}))

app.use(bodyParser.json());

const publicVapidKey = "BJthRQ5myDgc7OSXzPCMftGw-n16F7zQBEN7EUD6XxcfTTvrLGWSIG7y_JxiWtVlCFua0S8MTB5rPziBqNx1qIo";
const privateVapidKey = "3KzvKasA2SoCxsp0iIG_o9B0Ozvl1XDwI63JRKNIWBM";

webpush.setVapidDetails(
    "mailto:test@test.com",
    publicVapidKey,
    privateVapidKey
);

let subscription

// Subscribe Route
app.post("/subscribe", (req, res) => {
    // Get pushSubscription object

    console.log(`Got following request ${JSON.stringify(req.body)}`)

    subscription = req.body;

    // Send 201 - resource created
    res.status(201).json({});
});

app.post('/product', (req, res) => {

    const newProduct = req.body
    console.log(`New product created: ${JSON.stringify(newProduct)}`)
    const sub = JSON.parse('{"endpoint":"https://fcm.googleapis.com/fcm/send/dgTd0yOnHtI:APA91bG-NZOEx1lkluUoU4v_DwN677MBYPNrvcHYtvIS1WjGxsAou_gwwot8Lk3dT26KHqFVFDgcm6aNnE4tmkGm4YD-LQDLGh54pzhudtLWCNfbvBU8u_4dVwlvCYn148xs6bnXeq86","expirationTime":null,"keys":{"p256dh":"BJmVgDaAZsN5TFhQCSfiIzbztiz89DPd8b__1AvAR6b1WnNWV27Zt1flRBksn5kWsGDkSZjpaJJ76n-S1z9FA6k","auth":"2XrbrUK5WE328h9HRTzaPg"}}')
    webpush.sendNotification(sub, JSON.stringify({ title: `New product created: ${newProduct.name}`, body: newProduct}))
    res.status(200).json({ sent: true })
})

const port = 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));